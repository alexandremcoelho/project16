from flask import Blueprint, render_template, request, redirect, current_app, jsonify
import secrets
from app.helpers.key import new_key
from flask_httpauth import HTTPTokenAuth
from werkzeug.security import generate_password_hash, check_password_hash

bp = Blueprint("signup_blueprint", __name__)
auth = HTTPTokenAuth(scheme='Bearer')
@auth.verify_token
def verify_token(token):
    from app.models import user as users
    try:
        user = users.query.filter_by(api_key=token).first()
        return user.api_key
    except:
        ...


@bp.get("/api/signup")
def form():
    return render_template('signup.html'), 200

@bp.post("/api")
def signup():
    from app.models import user
    key = new_key()
    user = user(name=request.form['name'],last_name=request.form['last_name'], email=request.form['email'], password=request.form['password'], api_key=key)
    current_app.db.session.add(user)
    current_app.db.session.commit()
    return redirect("/api/signup")

@bp.get("/api")
@auth.login_required
def user_info():
    from app.models import user as users
    user = users.query.filter_by(api_key=auth.current_user()).first()
    data = user.serialized
    return jsonify(data)

