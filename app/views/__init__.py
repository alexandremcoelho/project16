from flask import Flask 

def init_app(app: Flask):
    from .signup import bp as bp_signup

    app.register_blueprint(bp_signup)
