import secrets
import string

def new_key():
    return secrets.token_urlsafe(16)