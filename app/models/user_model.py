from . import db
from werkzeug.security import check_password_hash, generate_password_hash


class user(db.Model):

    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(127), nullable=False)
    last_name = db.Column(db.String(511), nullable=False)
    email = db.Column(db.String(255), nullable=False, unique=True)
    password_hash = db.Column(db.String(511), nullable=False)
    api_key = db.Column(db.String(511), nullable=False)


    @property
    def password(self):
        raise AttributeError("Password is not acessible")

    @password.setter
    def password(self, password_to_hash):
        self.password_hash = generate_password_hash(password_to_hash)

    def check_password(self, password_to_compare):
        return check_password_hash(self.password_hash, password_to_compare)
        
    @property
    def serialized(self):
        return {"id":self.id, "name": self.name, "last_name": self.last_name,"email": self.email, "api_key": self.api_key }